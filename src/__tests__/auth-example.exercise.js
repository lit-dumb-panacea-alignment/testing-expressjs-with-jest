import axios from 'axios'
import {resetDb} from 'utils/db-utils'
import * as generate from 'utils/generate'
import {getData, handleRequestFailure} from 'utils/async'
import startServer from '../start'

let server, baseURL, api

beforeAll(async () => {
  server = await startServer()
  baseURL = `http://localhost:${server.address().port}/api`
  api = axios.create({baseURL})
  api.interceptors.response.use(getData, handleRequestFailure)
})

afterAll(() => server.close())

beforeEach(() => resetDb())

test('auth flow', async () => {
  const {username, password} = generate.loginForm()

  // register
  const rResult = await axios.post(`${baseURL}/auth/register`, {
    username,
    password,
  })
  expect(rResult.data.user).toEqual({
    token: expect.any(String),
    id: expect.any(String),
    username,
  })

  // login
  const lResult = await axios.post(`${baseURL}/auth/login`, {
    username,
    password,
  })
  expect(lResult.data.user).toEqual(rResult.data.user)

  // authenticated request
  const mResult = await axios.get(`${baseURL}/auth/me`, {
    headers: {
      Authorization: `Bearer ${lResult.data.user.token}`,
    },
  })
  expect(mResult.data.user).toEqual(lResult.data.user)
})
